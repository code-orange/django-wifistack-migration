from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt

from django_cdstack_models.django_cdstack_models.models import CmdbHost, CmdbVarsHost
from django_simple_notifier.django_simple_notifier.plugin_zammad import (
    send as send_ticket,
)


@csrf_exempt
def migrate(request):
    node = request.GET.get("node", "FAIL")
    token = request.GET.get("token", "FAIL")

    template = loader.get_template("django_wifistack_migration/thank_you.html")
    template_success = loader.get_template(
        "django_wifistack_migration/template_success.txt"
    )

    if node == "FAIL" or token == "FAIL":
        return HttpResponse(status=408)

    try:
        node_host = CmdbHost.objects.get(hostname=node + ".nodes.managed-infra.com")
    except CmdbHost.DoesNotExist:
        return HttpResponse(status=408)

    if node_host.cmdbvarshost_set.get(name="migration_token").value == token:
        migration_flag = node_host.cmdbvarshost_set.get(name="migration_flag")
        migration_flag.value = "accepted"
        migration_flag.save()

        ticket_id = None

        try:
            migration_ticket = node_host.cmdbvarshost_set.get(name="migration_ticket")
            ticket_id = migration_ticket.value
        except CmdbVarsHost.DoesNotExist:
            migration_ticket = CmdbVarsHost(
                host_rel=node_host, name="migration_ticket", value="None"
            )
            migration_ticket.save(force_insert=True)

        recipients = list()

        for contact in node_host.cmdbnotifieremailhost_set.all():
            recipients.append(contact)

        subject = "Willkommen bei Dolphin Connect!"

        migration_ticket.value = send_ticket(
            notifier_list=recipients,
            subject=subject,
            text=template_success.render(),
            group="VfN-NRW e.V.",
            note=node_host.hostname,
            ticket_id=ticket_id,
            main_address=None,
        )
        migration_ticket.save()

    return HttpResponse(status=200, content=template.render(dict(), request))
