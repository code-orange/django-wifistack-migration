Sehr geehrte Damen und Herren, liebe Freifunkerinnen und Freifunker,

im Laufe des Jahres haben wir Sie bereits darüber informiert, dass der Verbund freier Netzwerke NRW e.V. seinen Betrieb zum 31.12.2019 einstellen wird.

Dies hat zur Folge, dass Ihr Freifunk-Sender (auch Knoten genannt) nicht mehr funktionstüchtig sein wird:
Bezeichnung: {{ cmdb_host.title }} (ID: {{ node_hostname }})

Sie sind für diesen Sender als Kontakt hinterlegt.

Im Rahmen unserer Bemühungen hat sich Dolphin IT-Systeme e.K. (https://wifi.dolphin-connect.com/) dazu bereit erklärt, den Betrieb auch nach dem 31.12. sicherzustellen.

Dolphin IT betreibt bereits zahlreiche öffentliche WLAN-Netze, zum Beispiel:
Hückeswagen: https://rp-online.de/nrw/staedte/hueckeswagen/hueckeswagen-stadt-bringt-schnelles-stabiles-wlan-in-die-innenstadt_aid-39004379
Wipperfürth: https://www.wipperfuerth.de/wirtschaft-wohnen/freies-wlan.html
Lindlar: https://www.wifi-lindlar.de/vorstellung-unserer-kooperationspartner/
und viele weitere mehr.

Es ist ganz einfach in das neue Netzwerk zu wechseln, es sind keine technischen Kenntnisse notwendig.
Auch das neue Netzwerk ist für Sie kostenfrei - keine Einrichtungsgebühren, keine laufenden Kosten.

Um dem Wechsel zuzustimmen, müssen Sie nur dem folgenden Link folgen:
https://knoten-umzug.vfn-nrw.de/migrate?node={{ node_hostname }}&token={{ migration_token }}
(Der Token stellt sicher, dass nur Sie dem Umzug zustimmen können.)

Wenn Sie dem Wechsel NICHT zustimmen möchten, wird Ihr Sender automatisch ab 01.01.2020 offline gehen.
Zu diesem Zeitpunkt ist ein automatischer Wechsel leider nicht mehr möglich.

Mit freundlichen Grüßen
der Vorstand

Verbund freier Netzwerke NRW e.V.
Postfach 50 13 63
D-42929 Wermelskirchen
