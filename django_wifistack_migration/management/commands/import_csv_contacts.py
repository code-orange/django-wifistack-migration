import csv

from django.core.management.base import BaseCommand

from django_cdstack_models.django_cdstack_models.models import *


class Command(BaseCommand):
    help = "Import Meshviewer data into CMDB"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        file = open("data/freifunk-kontakt.csv", "rU")
        reader = csv.reader(file, delimiter=";")

        for row in reader:
            try:
                host = CmdbHost.objects.get(
                    hostname=row[0] + ".nodes.managed-infra.com"
                )
            except CmdbHost.DoesNotExist:
                continue

            print("Node: " + row[0] + ", " + row[1] + "\n")

            try:
                email_contact = CmdbNotifierEmailHost.objects.get(
                    host=host, email=row[1]
                )
            except CmdbNotifierEmailHost.DoesNotExist:
                email_contact = CmdbNotifierEmailHost(host=host, email=row[1])
                email_contact.save(force_insert=True)

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
