import random
import string

from django.core.management.base import BaseCommand
from django.template import loader

from django_cdstack_deploy.django_cdstack_deploy.func import get_host_vars
from django_cdstack_models.django_cdstack_models.models import *
from django_simple_notifier.django_simple_notifier.plugin_zammad import (
    send as send_ticket,
)


def randomString(stringLength=10):
    """Generate a random string of fixed length"""
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(stringLength))


class Command(BaseCommand):
    help = "Notify node owners"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        template = loader.get_template("django_wifistack_migration/template_opt_in.txt")

        all_wifi_nodes = CmdbHost.objects.filter(
            hostname__endswith=".nodes.managed-infra.com",
            inst_rel_id__in=(80, 84, 85, 86, 87, 88, 89, 90),
        )

        for node in all_wifi_nodes:
            try:
                migration_flag = node.cmdbvarshost_set.get(name="migration_flag")
            except CmdbVarsHost.DoesNotExist:
                migration_flag = CmdbVarsHost(
                    host_rel=node, name="migration_flag", value="true"
                )
                migration_flag.save(force_insert=True)

            if not migration_flag.value == "true":
                continue

            try:
                migration_token = node.cmdbvarshost_set.get(name="migration_token")
            except CmdbVarsHost.DoesNotExist:
                migration_token = CmdbVarsHost(
                    host_rel=node, name="migration_token", value=randomString(8)
                )
                migration_token.save(force_insert=True)

            ticket_id = None

            try:
                migration_ticket = node.cmdbvarshost_set.get(name="migration_ticket")
            except CmdbVarsHost.DoesNotExist:
                migration_ticket = CmdbVarsHost(
                    host_rel=node, name="migration_ticket", value="None"
                )
                migration_ticket.save(force_insert=True)

            if not migration_ticket.value == "None":
                ticket_id = migration_ticket.value

            recipients = list()

            internal_recipient = False

            for contact in node.cmdbnotifieremailhost_set.all():
                recipients.append(contact)
                if contact.email == "vorstand@vfn-nrw.de":
                    internal_recipient = True

            if not internal_recipient:
                internal_recipient_obj = CmdbNotifierEmailHost(
                    host=node,
                    email="vorstand@vfn-nrw.de",
                )
                internal_recipient_obj.save(force_insert=True)
                recipients.append(internal_recipient_obj)

            node_vars = get_host_vars(node)

            subject = "Ihr Freifunk-Sender (WLAN) - Wichtige Informationen"

            migration_ticket.value = send_ticket(
                notifier_list=recipients,
                subject=subject,
                text=template.render(node_vars),
                group="VfN-NRW e.V.",
                note=node.hostname,
                ticket_id=ticket_id,
                main_address=None,
            )
            migration_ticket.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
