import json

from django.core.management.base import BaseCommand

from django_cdstack_models.django_cdstack_models.models import *


class Command(BaseCommand):
    help = "Import Meshviewer data into CMDB"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        node_data = json.loads(open("data/meshviewer.json").read())

        for node in node_data["nodes"]:
            try:
                host = CmdbHost.objects.get(
                    hostname=node["node_id"] + ".nodes.managed-infra.com"
                )
            except CmdbHost.DoesNotExist:
                continue

            if "owner" in node:
                try:
                    email_contact = CmdbNotifierEmailHost.objects.get(
                        host=host, email=node["owner"]
                    )
                except CmdbNotifierEmailHost.DoesNotExist:
                    email_contact = CmdbNotifierEmailHost(
                        host=host, email=node["owner"]
                    )
                    email_contact.save(force_insert=True)

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
