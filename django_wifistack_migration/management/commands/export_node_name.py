import json

from django.core.management.base import BaseCommand

from django_cdstack_models.django_cdstack_models.models import *


class Command(BaseCommand):
    help = "Export node data from CMDB"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        nodes = CmdbHost.objects.filter(
            hostname__endswith=".nodes.managed-infra.com", inst_rel_id=80
        )

        for node in nodes:
            contacts_list = list()
            contacts = CmdbNotifierEmailHost.objects.filter(host=node)

            for contact in contacts:
                contacts_list.append(contact.email)

            print(node.hostname + ";" + node.title + ";" + ", ".join(contacts_list))

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
