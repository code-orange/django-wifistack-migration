import json

from django.core.management.base import BaseCommand

from django_cdstack_models.django_cdstack_models.models import *


class Command(BaseCommand):
    help = "Import Meshviewer data into CMDB"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        node_data = json.loads(open("data/nodelist.json").read())

        for node in node_data["nodes"]:
            try:
                host = CmdbHost.objects.get(
                    hostname=node["id"] + ".nodes.managed-infra.com"
                )
            except CmdbHost.DoesNotExist:
                host = CmdbHost(
                    hostname=node["id"] + ".nodes.managed-infra.com",
                    enabled=1,
                    inst_rel_id=1,
                )
                host.save()

            host.inst_rel_id = 75
            host.title = node["name"]

            if "position" in node:
                host.geoloc = {
                    "coordinates": (node["position"]["long"], node["position"]["lat"]),
                    "type": "Point",
                }
            host.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
